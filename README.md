## Installation

1. Add below code and save as  `.editorconfig` to `/static-template` 
```
    root = true
    [*]
    end_of_line = lf
    charset = utf-8
    indent_style = space
    indent_size = 2
    trim_trailing_whitespace = true
    insert_final_newline = true
    [*.{md,pug}]
    trim_trailing_whitespace = false
```
2. Add below code and save as `.stylelintrc` to `/static-template`
```
{
  "extends": "stylelint-config-standard",
  "rules": {
    "at-rule-no-unknown": [ true, {
      ignoreAtRules: [
        'extend', 'at-root', 'debug', 'nth',
        'warn', 'error', 'if', 'else', 'stylelint-commands',
        'for', 'each', 'while', 'mixin',
        'include', 'content', 'return', 'function'
      ]
    }],
    "at-rule-empty-line-before": [
      "always",
        {
          "except": ["inside-block"],
          "ignore": ["blockless-after-blockless", "after-comment"]
        }
    ],
    "no-invalid-double-slash-comments": null,
    "comment-empty-line-before": [
      "always",
        {
          "ignore": ["after-comment", "stylelint-commands"]
        }
    ],
    "rule-empty-line-before": [ "always-multi-line",
      {
        "except": ["inside-block"],
        "ignore": ["after-comment"]
      }
    ],
    "selector-list-comma-newline-after": ["always-multi-line"],
    "declaration-empty-line-before": ["never"]
  }
}

```
3. Install all package using `npm install`, if you don't have yet npm go here https://www.npmjs.com/get-npm.
4. After installation of `npm`, use `gulp`.
